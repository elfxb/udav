// Fill out your copyright notice in the Description page of Project Settings.


#include "UdavBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


// Sets default values
AUdavBase::AUdavBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 50.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;
	DeltaSpeed = 0.005;
}

// Called when the game starts or when spawned
void AUdavBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(1);
}

// Called every frame
void AUdavBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void AUdavBase::AddSnakeElement(int ElementsNum)
{	
	for (int i = 0; i < ElementsNum; i++) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
		else {
			NewSnakeElem->SetActorHiddenInGame(true);
		}
	}
	SetActorTickInterval(0.5 - DeltaSpeed);
	DeltaSpeed += 0.005;
}

void AUdavBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	MovementSpeed = ElementSize;
	switch (LastMoveDirection) 
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0] -> AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void AUdavBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* Interactableinterface = Cast<IInteractable>(Other);
		if (Interactableinterface) {
			Interactableinterface->interact(this, bIsFirst);
		}
	}
}

//Aa3?Aa3?