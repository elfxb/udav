// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UdavGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UDAV_API AUdavGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
