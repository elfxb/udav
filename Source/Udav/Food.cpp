// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "UdavBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include <stdlib.h>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

int AFood::GetRandom(int max_value, int min_value, int step)
{
	int random_value = (rand() % ((++max_value - min_value) / step)) * step + min_value;
	return random_value;
}

void AFood::AddFood(int FoodAmount)
{
	//500 -1100 0
	//-500 1100 0
	for (int i = 0; i < FoodAmount; i++) {
		FVector NewLocation(static_cast<float>(GetRandom(500, -500, 50)), static_cast<float>(GetRandom(1100, -1100, 50)), 0);
		FTransform NewTransform(NewLocation);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	}
}

void AFood::interact(AActor* Interactor, bool bIsHead)
{
	AddFood();
	if (bIsHead) {
		auto Snake = Cast<AUdavBase>(Interactor);
			if (IsValid(Snake)) {
				Snake->AddSnakeElement();
			}
	}
	
}